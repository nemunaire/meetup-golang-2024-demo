package main

import (
	"embed"
	"io"
	"log"
	"net/http"
)

//go:embed index.html style.css
var staticfiles embed.FS

func main() {
	http.HandleFunc("GET /containers", func(w http.ResponseWriter, r *http.Request) {
		rd, err := curl("/var/run/docker.sock", "GET", "http://d/v1.43/containers/json?all=true", nil)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			io.Copy(w, rd)
		}
	})

	http.HandleFunc("POST /containers", func(w http.ResponseWriter, r *http.Request) {
		rd, err := curl("/var/run/docker.sock", "POST", "http://d/v1.43/containers/create", []byte(`{"Image": "alpine:latest", "Cmd": ["echo", "Hello Go Meetup!"]}`))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else {
			io.Copy(w, rd)
		}
	})

	http.Handle("GET /", http.FileServer(http.FS(staticfiles)))

	log.Fatal(http.ListenAndServe(":8080", nil))
}
