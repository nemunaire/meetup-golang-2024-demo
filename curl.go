package main

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"net/http"
)

func curl(socket, method, path string, body []byte) (io.ReadCloser, error) {
	socketDial := func(proto, addr string) (conn net.Conn, err error) {
		return net.Dial("unix", socket)
	}

	tr := &http.Transport{
		Dial: socketDial,
	}
	client := &http.Client{
		Transport: tr,
	}

	req, err := http.NewRequest(method, path, bytes.NewReader(body))
	if err != nil {
		return nil, fmt.Errorf("Error creating request: %w\n", err)
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("Error performing request: %w\n", err)
	}
	return resp.Body, nil
}
